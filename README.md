# minimal font

最低限度のフォントデータをrgba画像bufferとして出力する

## 使い方
`minimal_font::get()`でbufferを生成する

フォントは1文字 8x4 dotのサイズ

フォントデータは[美咲フォント](https://littlelimit.net/misaki.htm)を使用させて頂いた
```cpp
const auto data{minimal_font::get()};
// data.img    -> std::vector<std::uint8_t>型の画像buffer
//                レイアウトは r, g, b, a, r, g, b, a, ...
// data.width  -> 画像の幅
// data.height -> 画像の高さ
```
>![img_0](img/img_0.png) zoom x4

---

文字色と背景色を変更する
```cpp
minimal_font::config cnf;
// 背景色を不透明シアンに
cnf.bg.r = 0x00;
cnf.bg.g = cnf.bg.b = cnf.bg.a = 0xFF;
// 文字色を不透明赤に
cnf.color.r = cnf.color.a = 0xFF;
cnf.color.g = cnf.color.b = 0x00;

const auto data{minimal_font::get(cnf)};
```
>![alt text](img/img_1.png) zoom x2

---

y方向に反転させる
```cpp
minimal_font::config cnf;
cnf.flip_y = true;

const auto data{minimal_font::get(cnf)};
```
>![alt text](img/img_2.png) zoom x2

---

1行あたりの文字数を変更する
```cpp
minimal_font::config cnf;
// 1行あたりの文字数を総文字数と等しくする -> 1行に全文字出力する
cnf.column_count = cnf.char_count;

const auto data{minimal_font::get(cnf)};
```
>![alt text](img/img_3.png)
>zoom x1

---

自前のデータを使用する
```cpp
//     @           @          0 0 1 0 0 0 0 0 1 0 0 0      0010 0000 1000
//       @       @            0 0 0 1 0 0 0 1 0 0 0 0      0001 0001 0000
//     @ @ @ @ @ @ @          0 0 1 1 1 1 1 1 1 0 0 0      0011 1111 1000
//   @ @   @ @ @   @ @    ->  0 1 1 0 1 1 1 0 1 1 0 0  ->  0110 1110 1100
// @ @ @ @ @ @ @ @ @ @ @  ->  1 1 1 1 1 1 1 1 1 1 1 0  ->  1111 1111 1110
// @   @ @ @ @ @ @ @   @      1 0 1 1 1 1 1 1 1 0 1 0      1011 1111 1010
// @   @           @   @      1 0 1 0 0 0 0 0 1 0 1 0      1010 0000 1010
//       @ @   @ @            0 0 0 1 1 0 1 1 0 0 0 0      0001 1011 0000
//                                                          |    |    |
// 0010 0001 0011 0110 1111 1011 1010 0001  <---------------+    |    |
// 0000 0001 1111 1110 1111 1111 0000 1011  <--------------------+    |
// 1000 0000 1000 1100 1110 1010 1010 0000  <-------------------------+

constexpr minimal_font::fontcode font[]{
  0b0010'0001'0011'0110'1111'1011'1010'0001,
  0b0000'0001'1111'1110'1111'1111'0000'1011,
  0b1000'0000'1000'1100'1110'1010'1010'0000
};

minimal_font::config cnf;
cnf.char_count = cnf.column_count = 3;
cnf.bg.r = cnf.bg.g = cnf.bg.b = 0x00;
cnf.bg.a = 0xFF;
cnf.color.r = 0x00;
cnf.color.g = cnf.color.b = cnf.color.a = 0xFF;

const auto data{minimal_font::get(cnf, font)};
```
>![alt text](img/img_4.png) zoom x4