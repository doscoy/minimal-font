#include <filesystem>
#include <fstream>
#include <iostream>

#include "minimal_font.hpp"

void Save(const std::filesystem::path& argv_0,
          const std::filesystem::path& file_name,
          const minimal_font::data& data) {
  const std::filesystem::path out_path{argv_0.parent_path() / file_name};

  std::ofstream ofs{out_path, std::ios_base::binary};
  if (ofs.is_open()) {
    ofs.write(reinterpret_cast<const char*>(data.img.data()), data.img.size());
    std::cout << "save to: " << out_path;
    std::cout << " (" << data.width << ", " << data.height << ")\n";
  }
}

int main([[maybe_unused]] int argc, char** argv) {
  // basic usage
  {
    auto data{minimal_font::get()};
    Save(argv[0], "img_0.rgba", data);
  }

  // color config
  {
    minimal_font::config cnf;
    cnf.bg.r = 0x00;
    cnf.bg.g = cnf.bg.b = cnf.bg.a = 0xFF;
    cnf.color.r = cnf.color.a = 0xFF;
    cnf.color.g = cnf.color.b = 0x00;

    auto data{minimal_font::get(cnf)};
    Save(argv[0], "img_1.rgba", data);
  }

  // flip y
  {
    minimal_font::config cnf;
    cnf.flip_y = true;

    auto data{minimal_font::get(cnf)};
    Save(argv[0], "img_2.rgba", data);
  }

  // transform
  {
    minimal_font::config cnf;
    cnf.column_count = cnf.char_count;

    auto data{minimal_font::get(cnf)};
    Save(argv[0], "img_3.rgba", data);
  }

  // custom font
  {
    constexpr minimal_font::fontcode font[]{
      0b0010'0001'0011'0110'1111'1011'1010'0001,
      0b0000'0001'1111'1110'1111'1111'0000'1011,
      0b1000'0000'1000'1100'1110'1010'1010'0000
    };
    minimal_font::config cnf;
    cnf.char_count = cnf.column_count = 3;
    cnf.bg.r = cnf.bg.g = cnf.bg.b = 0x00;
    cnf.bg.a = 0xFF;
    cnf.color.r = 0x00;
    cnf.color.g = cnf.color.b = cnf.color.a = 0xFF;

    auto data{minimal_font::get(cnf, font)};
    Save(argv[0], "img_4.rgba", data);
  }

  return 0;
}