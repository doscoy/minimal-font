#pragma once

#include <array>
#include <cmath>
#include <cstdint>
#include <vector>

namespace minimal_font {

struct config {
  struct rgba {
    std::uint8_t r, g, b, a;
  };
  int char_count{128};
  int column_count{16};
  rgba color{0x00, 0x00, 0x00, 0xff};
  rgba bg{0x00, 0x00, 0x00, 0x00};
  bool flip_y{false};
};

struct data {
  int width;
  int height;
  std::vector<std::uint8_t> img;
};

using fontcode = std::uint32_t;

namespace detail {

//
//   |--4--|
// -
// |    @            0 1 0 0
// |  @   @          1 0 1 0
// |  @   @      \   1 0 1 0      \   0b0100'1010'1010'1110'1010'1010'0000'0000
// 8  @ @ @  ---- \  1 1 1 0  ---- \                      ||
// |  @   @  ---- /  1 0 1 0  ---- /                     \||/
// |  @   @      /   1 0 1 0      /                       \/
// |                 0 0 0 0                          0x4aaeaa00
// |                 0 0 0 0
// -
//

// clang-format off
inline constexpr fontcode kFont[]{
    // using misaki_font by Num Kadoma
    // https://littlelimit.net/misaki.htm
    0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u,
    0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u, 0x00000000u,
    0x00000000u, 0x44440400u, 0xaa000000u, 0xaeaaea00u, 0x4ec6e400u, 0x08248200u, 0x4a4a8600u, 0x48000000u, 0x24444420u, 0x84444480u, 0x4e4a0000u, 0x044e4400u, 0x00000480u, 0x000e0000u, 0x00000400u, 0x00248000u,
    0x04aea400u, 0x04c44e00u, 0x0c248e00u, 0x0c242c00u, 0x026ae200u, 0x0e8c2c00u, 0x068ca400u, 0x0e244400u, 0x04a4a400u, 0x04a62c00u, 0x00400400u, 0x00400480u, 0x02484200u, 0x00e0e000u, 0x08424800u, 0x4a240400u,
    0x4a26a400u, 0x4aaeaa00u, 0xcacaac00u, 0x68888600u, 0xcaaaac00u, 0xe8c88e00u, 0xe8c88800u, 0x688aa600u, 0xaaaeaa00u, 0xe4444e00u, 0x2222a400u, 0xaacaaa00u, 0x88888e00u, 0xaeeaaa00u, 0xcaaaaa00u, 0x4aaaa400u,
    0xcaac8800u, 0x4aaaa420u, 0xcaacaa00u, 0x68422c00u, 0xe4444400u, 0xaaaaae00u, 0xaaaac800u, 0xaaaeea00u, 0xaa44aa00u, 0xaa444400u, 0xe2448e00u, 0x64444460u, 0xa4e4e400u, 0xc44444c0u, 0x4a000000u, 0x000000e0u,
    0x42000000u, 0x006aa600u, 0x88caac00u, 0x00688600u, 0x226aa600u, 0x006e8600u, 0x64e44400u, 0x006a62c0u, 0x88caaa00u, 0x40444400u, 0x40444480u, 0x88acaa00u, 0xc4444400u, 0x00ceea00u, 0x00caaa00u, 0x004aa400u,
    0x00caac80u, 0x006aa620u, 0x00ac8800u, 0x006c6c00u, 0x04e44600u, 0x00aaae00u, 0x00aac800u, 0x00aaee00u, 0x00a44a00u, 0x00aa62c0u, 0x00e24e00u, 0x24484420u, 0x44444440u, 0x84424480u, 0xe0000000u, 0x00000000u};
// clang-format on

inline constexpr auto kCharWidth{4};
inline constexpr auto kCharHeight{8};

std::array<std::uint8_t, kCharHeight> split_code(const fontcode code,
                                                 const config& config) {
  if (not config.flip_y) {
    return std::array<std::uint8_t, kCharHeight>{
        static_cast<uint8_t>((code & 0xf0000000) >> 28),
        static_cast<uint8_t>((code & 0x0f000000) >> 24),
        static_cast<uint8_t>((code & 0x00f00000) >> 20),
        static_cast<uint8_t>((code & 0x000f0000) >> 16),
        static_cast<uint8_t>((code & 0x0000f000) >> 12),
        static_cast<uint8_t>((code & 0x00000f00) >> 8),
        static_cast<uint8_t>((code & 0x000000f0) >> 4),
        static_cast<uint8_t>((code & 0x0000000f) >> 0)};
  } else {
    return std::array<std::uint8_t, kCharHeight>{
        static_cast<uint8_t>((code & 0x0000000f) >> 0),
        static_cast<uint8_t>((code & 0x000000f0) >> 4),
        static_cast<uint8_t>((code & 0x00000f00) >> 8),
        static_cast<uint8_t>((code & 0x0000f000) >> 12),
        static_cast<uint8_t>((code & 0x000f0000) >> 16),
        static_cast<uint8_t>((code & 0x00f00000) >> 20),
        static_cast<uint8_t>((code & 0x0f000000) >> 24),
        static_cast<uint8_t>((code & 0xf0000000) >> 28)};
  }
}

int index_to_pixel_head(const int index, const config& config) {
  if (not config.flip_y) {
    const auto row{index / config.column_count};
    const auto col{index % config.column_count};
    return kCharWidth * kCharHeight * config.column_count * row +
           kCharWidth * col;
  } else {
    const auto row_cnt{
        static_cast<int>(std::ceil(config.char_count / config.column_count))};
    const auto row{row_cnt - (index / config.column_count) - 1};
    const auto col{index % config.column_count};
    return kCharWidth * kCharHeight * config.column_count * row +
           kCharWidth * col;
  }
}

void decode(std::uint8_t* buffer, const int index, const fontcode code,
            const config& config) {
  config::rgba* cast_buffer{reinterpret_cast<config::rgba*>(buffer)};
  const auto split{split_code(code, config)};
  const auto base{index_to_pixel_head(index, config)};
  for (int i = 0; i < kCharHeight; ++i) {
    const auto head{base + i * kCharWidth * config.column_count};
    for (int j = 0; j < kCharWidth; ++j) {
      cast_buffer[head + j] =
          split[i] & (0b1000 >> j) ? config.color : config.bg;
    }
  }
}

}  // namespace detail

data get(const config& cnf = config{},
         const fontcode* font_data = detail::kFont) {
  const int width{cnf.column_count * detail::kCharWidth};
  const int height{static_cast<int>(
      std::ceil(cnf.char_count / cnf.column_count) * detail::kCharHeight)};
  data data{width, height, {}};
  data.img.resize(detail::kCharWidth * detail::kCharHeight * cnf.char_count * 4);

  for (int i = 0; i < cnf.char_count; ++i) {
    detail::decode(data.img.data(), i, font_data[i], cnf);
  }

  return data;
}

}  // namespace minimal_font